
const ormconfig={
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: false,
  migrations: ['/*{.ts,.js}'],
  migrationsTableName: 'migrations_typeorm',
  migrationsRun: false,
};

module.exports = ormconfig;