import { Injectable } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { Country } from './country.entity';
import { CountryRepository } from './country.repository';

@Injectable()
export class CountryService {
    constructor(private repo:CountryRepository){}

@ApiOkResponse({type:[Country]})    
async getAllCountry():Promise<Country[]>{
 return this.repo.find();
} 
}
