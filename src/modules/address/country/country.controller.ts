import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { Country } from './country.entity';
import { CountryService } from './country.service';

@Controller('country')
export class CountryController {
    constructor(private countryService:CountryService){}


@Get()
@ApiOkResponse({type:[Country]})
async getCities():Promise<Country[]>{
    return this.countryService.getAllCountry();
   }
}
