import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CountryController } from './country.controller';
import { Country } from './country.entity';
import { CountryRepository } from './country.repository';
import { CountryService } from './country.service';

@Module({
    imports:[TypeOrmModule.forFeature([Country,CountryRepository])],
    providers: [CountryService],
    controllers: [CountryController],
    exports:[TypeOrmModule,CountryService]
  })
export class CountryModule {}
