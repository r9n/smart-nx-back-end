import { MAX_COUNTRY_NAME_LENGHT } from 'src/config/constraints';
import { BaseEntity } from 'src/modules/base.entity';
import { Entity, Column, OneToMany } from 'typeorm';
import { State } from '../state/state.entity';

@Entity()
export class Country extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_COUNTRY_NAME_LENGHT })
  country: string;

  @OneToMany((type) => State, (state) => state.country,{cascade:true})
  states: State[];
}
