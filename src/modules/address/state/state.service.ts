import { Injectable } from '@nestjs/common';
import { State } from './state.entity';
import { StateRepository } from './state.repository';

@Injectable()
export class StateService {
constructor(private repo:StateRepository){}
async getStateByCountry(country:string):Promise<State[]>{
        return this.repo.find({where:{country:{id:country}},relations:['country']});
}    
}
