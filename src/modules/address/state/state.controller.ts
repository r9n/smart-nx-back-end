import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { State } from './state.entity';
import { StateService } from './state.service';

@Controller('state')
export class StateController {

constructor(private service:StateService){}

@Get('/by-country/:id')
@ApiOkResponse({type:[State]})
async getStates(@Param('id') countrId:string):Promise<State[]>{
    return this.service.getStateByCountry(countrId);
}
  }
