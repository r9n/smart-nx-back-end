import { MAX_STATE_LENGHT } from 'src/config/constraints';
import { BaseEntity } from 'src/modules/base.entity';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { City } from '../city/city.entity';
import { Country } from '../country/country.entity';

@Entity()
export class State extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_STATE_LENGHT })
  state: string;

  @ManyToOne((type) => Country, (country) => country.states)
  country: Country;

  @OneToMany((type) => City, (city) => city.state,{cascade:true})
  cities: City[];
}
