import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../base.entity';
import { User } from '../user/user.entity';
import { City } from './city/city.entity';

@Entity()
export class Address extends BaseEntity {
  @Column({ type: 'varchar' })
  street: string;

  @Column({ type: 'varchar' })
  postalCode: string;

  @ManyToOne((type) => City, (city) => city.address)
  City: City;

  @OneToMany((type) => User, (user) => user.address,)
  users: User;

  constructor(partial:Partial<Address>){
    super();
    Object.assign(this,partial);
  }
}
