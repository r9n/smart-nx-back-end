import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Address } from './address.entity';
import { AddressRepository } from './address.repository';
import { UserModule } from '../user/user.module';
import { CityModule } from './city/city.module';

@Module({
  imports:[TypeOrmModule.forFeature([Address,AddressRepository]),UserModule,CityModule],
  providers: [AddressService],
  controllers: [AddressController],
  exports:[TypeOrmModule,AddressService]
})
export class AddressModule {}
