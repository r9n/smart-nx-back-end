import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CityController } from './city.controller';
import { City } from './city.entity';
import { CityRepository } from './city.repository';
import { CityService } from './city.service';

@Module({
    imports:[TypeOrmModule.forFeature([City,CityRepository])],
    providers: [CityService],
    controllers: [CityController],
    exports:[TypeOrmModule,CityService]
  })
export class CityModule {}
