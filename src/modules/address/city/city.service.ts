import { Injectable } from '@nestjs/common';
import { City } from './city.entity';
import { CityRepository } from './city.repository';

@Injectable()
export class CityService {
constructor(private repo:CityRepository){}
async getCityByState(stateId:string):Promise<City[]>{
 return this.repo.find({where:{state:{id:stateId}},relations:['state']});
} 
}
