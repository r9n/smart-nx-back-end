import { MAX_CITY_NAME_LENGHT } from 'src/config/constraints';
import { BaseEntity } from 'src/modules/base.entity';
import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { Address } from '../address.entity';
import { State } from '../state/state.entity';

@Entity()
export class City extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_CITY_NAME_LENGHT })
  city: string;

  @ManyToOne((type) => State, (state) => state.cities)
  state: State;

  @OneToMany((type) => Address, (address) => address.City,{cascade:true})
  address: Address[];
}
