import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { City } from './city.entity';
import { CityService } from './city.service';

@Controller('city')
export class CityController {
   constructor(private service:CityService){}
    
 @Get('/by-state/:id')
 @ApiOkResponse({type:[City]})
async getCities(@Param('id') stateId:string):Promise<City[]>{
 return this.service.getCityByState(stateId);
}

}
