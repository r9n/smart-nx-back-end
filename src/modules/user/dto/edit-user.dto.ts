import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import {
    MAX_USER_EMAIL_LENGHT,
    MAX_USER_NAME_LENGHT,
    MIN_USER_EMAIL_LENGHT,
    MIN_USER_NAME_LENGHT,
  } from 'src/config/constraints';
import { Address } from 'src/modules/address/address.entity';
  
export class EditUserDto{

    @ApiProperty({type:String, minLength:MIN_USER_NAME_LENGHT,maxLength:MAX_USER_NAME_LENGHT,required:true})
    @IsString()
    @MinLength(MIN_USER_NAME_LENGHT)
    @MaxLength(MAX_USER_NAME_LENGHT)
    @IsNotEmpty()
    name: string;
    
    @ApiProperty({type:String, minLength:MIN_USER_NAME_LENGHT,maxLength:MAX_USER_EMAIL_LENGHT,required:true})
    @IsString()
    @MinLength(MIN_USER_EMAIL_LENGHT)
    @MaxLength(MAX_USER_EMAIL_LENGHT)
    @IsNotEmpty()
    email: string;

    @ApiProperty({type: Address,required:false})
    address: Address;
  }