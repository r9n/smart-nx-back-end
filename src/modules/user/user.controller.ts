import { Body, Controller, Get, Param, Post, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse } from '@nestjs/swagger';
import { avatarsFolder, tempAvatarsFolder } from 'src/config/constraints';
import { avatarImageCanotBeEmpty, avatarNotFound, fileTooBig, userAlreadyActivated, userAlreadyDeactivated, userNotFound } from '../messages/error.messages';
import { EditUserDto } from './dto/edit-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

@Post('/edit/:id')
@ApiNotFoundResponse({description:userNotFound})
@ApiCreatedResponse({type:User})
async editUser(@Body() dto:EditUserDto,@Param('id') userId:string):Promise<User>{
  return this.userService.editUser(dto,userId);
}

@Post('deactivate/:id')
@ApiCreatedResponse({type:User})
@ApiNotFoundResponse({description:userNotFound})
@ApiBadRequestResponse({description:userAlreadyDeactivated})
async deactivate(@Param('id') userId:string):Promise<User>{
  return this.userService.deactivate(userId);
}

@Post('/reactivate/:id')
@ApiCreatedResponse({type:User})
@ApiNotFoundResponse({description:userNotFound})
@ApiBadRequestResponse({description:userAlreadyActivated})
async reactivate(@Param('id') userId:string):Promise<User>{
  return this.userService.reactivateUser(userId);
}

@Post('/avatar/:id')
@ApiCreatedResponse()
@ApiNotFoundResponse({description:`${userAlreadyDeactivated},${userNotFound}`})
@ApiBadRequestResponse({description:`${fileTooBig},${avatarImageCanotBeEmpty}`})
@UseInterceptors(FileInterceptor('file',{dest:tempAvatarsFolder}))
uploadAvatar(@UploadedFile() avatarFile: Express.Multer.File,@Param('id') userId:string):Promise<void> {
  return this.userService.saveAvatar(avatarFile,userId);
}



@Get('/avatar/:id')
@ApiOkResponse({type:'avatar'})
@ApiNotFoundResponse({description:`${userAlreadyDeactivated}, ${userNotFound}, ${avatarNotFound}`})
async getAvatar(@Param('id') userId:string,@Res() res){
 const avatarPath = await this.userService.getAvatarPath(userId);
 res.sendFile(avatarPath,{root:avatarsFolder});
}





}
