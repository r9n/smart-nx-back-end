import { from } from 'rxjs';
import {
  MAX_USER_EMAIL_LENGHT,
  MAX_USER_NAME_LENGHT,
} from 'src/config/constraints';
import { Column, Entity, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { Address } from '../address/address.entity';
import { BaseEntity } from '../base.entity';
import { Card } from '../card/card.entity';
import * as bcrypt from 'bcrypt';
import { EditUserDto } from './dto/edit-user.dto';
import { Roles } from 'src/enums/roles.enum';

@Entity()
export class User extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_USER_NAME_LENGHT })
  name: string;

  @Column({ type: 'varchar', length: MAX_USER_EMAIL_LENGHT })
  email: string;

  @Column({ type: 'boolean' })
  isActive: boolean;

  @OneToMany((type) => Card, (card) => card.user,{cascade:true})
  card: Card[];

  @ManyToOne((type) => Address, (address) => address.users)
  address: Address;

  @Column({ type: 'varchar'})
  passwordHash: string;

  @Column({ type: 'varchar' })
  salt: string;

  @Column({type:'enum',enum:Roles})
  role:Roles

  constructor(partial:Partial<User>){
    super();
    Object.assign(this,partial);
  }

  async validationPassword(password: string): Promise<boolean> {
		const hash = await bcrypt.hash(password, this.salt);
    console.log(hash.toString() == this.passwordHash.toString());
		return (hash.toString() == this.passwordHash.toString());
	}

  fromDto(dto:EditUserDto):void{
    this.name = dto.name ? dto.name:this.name;
    this.email = dto.email? dto.email:this.email;
    this.address = dto.address? dto.address:this.address;
  }
}
