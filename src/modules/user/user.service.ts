import { Injectable, NotFoundException, InternalServerErrorException, ForbiddenException, BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { plainToClass } from "class-transformer";
import { avatarsFolder, maxAvatarSize, MegaByte } from "src/config/constraints";
import { SigninDto } from "../auth/dto/signin.dto";
import { avatarImageCanotBeEmpty, avatarNotFound, fileTooBig, userDeactivated, userNotFound } from "../messages/error.messages";
import { EditUserDto } from "./dto/edit-user.dto";
import { User } from "./user.entity";
import * as fs from 'fs';
import { UserRepository } from "./user.repository";

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserRepository)
        private userRepository:UserRepository
    ){}

    async save(user:User):Promise<User> {
        return this.userRepository.save(user);
    }

    async findUserByEmail(email:string):Promise<User>{
        return this.userRepository.findOne({email},{relations:['card','address']});
    }

    async findUserById(id:string):Promise<User>{
        const rawUser:User[] = await this.userRepository.query(`select * from  "user" u where u.id = '${id}'`);
        if(rawUser.length === 0){
            throw new NotFoundException(userNotFound);
        }
        const user = plainToClass(User,rawUser[0]); 
        return user;
    }
    

    async validateUserPassword(
		dto: SigninDto,
	): Promise<Boolean> {
		const { email, password: password } = dto;
		const user = await this.userRepository.findOne(
			{ email },
		);

		  return await user.validationPassword(password);
		
	}


    async editUser(dto:EditUserDto,userId:string):Promise<User> {

        const databaseUser = await this.userRepository.findOne({id:userId});

        if(!databaseUser){
            throw new NotFoundException(userNotFound);
        }

        databaseUser.fromDto(dto);
    console.log(dto.address);
        return this.userRepository.save(databaseUser).catch((error)=>{
            throw new InternalServerErrorException(error.message);
        });

    }


    async reactivateUser(userId:string):Promise<User> {

        const databaseUser = await this.userRepository.findOne({id:userId});

        if(!databaseUser){
            throw new NotFoundException(userNotFound);
        }

        databaseUser.isActive = true;

        return this.userRepository.save(databaseUser).catch((error)=>{
            throw new InternalServerErrorException(error.message);
        });
    }

    async deactivate(userId:string):Promise<User> {

        const databaseUser = await this.userRepository.findOne({id:userId});

        if(!databaseUser){
            throw new NotFoundException(userNotFound);
        }

        databaseUser.isActive = false;

        return this.userRepository.save(databaseUser).catch((error)=>{
            throw new InternalServerErrorException(error.message);
        });

    }

    async saveAvatar(avatarFile:Express.Multer.File,userId:string):Promise<void>{
     const user = await this.userRepository.findOne(userId); 
     if(!user){
         throw new NotFoundException(userNotFound);
     }
     
     if(!user.isActive){
          throw new ForbiddenException(userDeactivated);
     }

     if(avatarFile.size == 0){
        throw new BadRequestException(avatarImageCanotBeEmpty)
     }

     if((avatarFile.size/MegaByte) > maxAvatarSize){
        fs.unlinkSync(avatarFile.path)
        throw new BadRequestException(fileTooBig);
     }

     const oldPath = avatarFile.path;
     const newPath = `${avatarsFolder}/${user.id}.png`.replace(oldPath,'');
     fs.rename(oldPath, newPath, function (err) {
    if (err){
        throw new InternalServerErrorException(err);
    }
    })
    }


    async getAvatarPath(userId:string):Promise<string>{
        const user = await this.userRepository.findOne(userId); 
        if(!user){
            throw new NotFoundException(userNotFound);
        }
        
        if(!user.isActive){
             throw new ForbiddenException(userDeactivated);
        }
        const avatarPath = `${avatarsFolder}/${user.id}.png`;
        
        if(!fs.existsSync(avatarPath)){
            throw new NotFoundException(avatarNotFound);
        }
        return `${user.id}.png`;
        
       }

}


