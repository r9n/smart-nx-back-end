import { ApiProperty } from "@nestjs/swagger";
import { Roles } from "src/enums/roles.enum";
import { Address } from "src/modules/address/address.entity";
import { Card } from "src/modules/card/card.entity";
import { OneToOne, Column } from "typeorm";

export class SigninResponseDto{

 @ApiProperty({type:String,description:'UUID'})
 id:string;

 @ApiProperty({type:String})
 email: string;

 @ApiProperty({type:Boolean})
 isActive: boolean;

 @ApiProperty({type:String})
 card: Card[];

 @ApiProperty({type:Address})
 address: Address;

 @Column({type:'enum',enum:Roles})
 role:Roles

 constructor(partial:Partial<SigninResponseDto>){
     Object.assign(this,partial);
 }
}