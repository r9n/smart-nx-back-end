import { ApiProperty } from "@nestjs/swagger";
import { MAX_USER_EMAIL_LENGHT, MIN_USER_EMAIL_LENGHT } from "src/config/constraints";
import {IsString,IsNotEmpty, MaxLength, MinLength} from 'class-validator'

export class SigninDto{

@ApiProperty({type:String,maxLength:MAX_USER_EMAIL_LENGHT,
        minLength:MIN_USER_EMAIL_LENGHT,required:true})
@IsString()
@IsNotEmpty()
@MinLength(MIN_USER_EMAIL_LENGHT)
@MaxLength(MAX_USER_EMAIL_LENGHT)
email:string;

@ApiProperty({type:String})
@IsString()
@IsNotEmpty()
password:string;
}