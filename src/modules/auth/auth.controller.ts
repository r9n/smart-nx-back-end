import { Controller, Post, Body } from "@nestjs/common";
import { ApiBody, ApiForbiddenResponse, ApiCreatedResponse } from "@nestjs/swagger";
import { emailAlreadyRegistered } from "../messages/error.messages";
import { User } from "../user/user.entity";
import { AuthService } from "./auth.service";
import { SigninResponseDto } from "./dto/signin-response.dto";
import { SigninDto } from "./dto/signin.dto";
import { CreateUserDto } from "./dto/signup.dto";

@Controller('auth')
export class AuthController {

constructor(private authService:AuthService){}

@Post('signup')
@ApiBody({ type: CreateUserDto })
@ApiForbiddenResponse({
    description: emailAlreadyRegistered
})
@ApiCreatedResponse({type:User})
async signup(@Body() dto:CreateUserDto):Promise<User>{
 return this.authService.signup(dto);   
}


@Post('signin')
@ApiBody({ type: SigninDto })
@ApiCreatedResponse({type:SigninResponseDto})
async singin(@Body() dto:SigninDto):Promise<SigninResponseDto>{
 return this.authService.signin(dto);   
}

}
