import { Injectable, ForbiddenException, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { emailAlreadyRegistered, userNotFound, wrongPassword } from "../messages/error.messages";
import { User } from "../user/user.entity";
import { UserService } from "../user/user.service";
import { SigninResponseDto } from "./dto/signin-response.dto";
import { SigninDto } from "./dto/signin.dto";
import { CreateUserDto } from "./dto/signup.dto";
import * as bcrypt from 'bcrypt';
import { Roles } from "src/enums/roles.enum";

@Injectable()
export class AuthService {

constructor(
    private userService:UserService,
){}


async signup(dto:CreateUserDto):Promise<User>{
const newUser:User = new User(dto);
const hasAccount = await this.userService.findUserByEmail(dto.email);

if(hasAccount){
   throw new ForbiddenException(emailAlreadyRegistered);
}

newUser.salt = await bcrypt.genSalt();


newUser.passwordHash = await this.createHasPassword(dto.password,newUser.salt);

newUser.isActive = true;

newUser.role = Roles.USER;


return this.userService.save(newUser).then((user)=>{
    user.passwordHash = '';
    return user;
})
.catch((error)=>{
    throw new InternalServerErrorException(error.message);
});
}


async signin(dto:SigninDto):Promise<SigninResponseDto> {

    const user = await this.userService.findUserByEmail(dto.email);

    if(!user){
        throw new NotFoundException(userNotFound);
    }
    
    if(await this.userService.validateUserPassword(dto) === true){
        user.salt ='';
        user.passwordHash = '';
        const signinReponseDto = new SigninResponseDto(user);
        return signinReponseDto;
    }

    throw new ForbiddenException(wrongPassword);

}

private async createHasPassword(password: string,salt:string): Promise<string> {
    return bcrypt.hash(password, salt);
}

}
