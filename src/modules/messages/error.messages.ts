import { maxAvatarSize } from "src/config/constraints";

export const emailAlreadyRegistered = 'This email is already registered in our database';

export const userNotFound = 'This user is not registered on our systems';

export const userAlreadyDeactivated = 'This user is already deactivated';

export const userAlreadyActivated = 'This user is already activated';

export const cardAlreadiRegistered = 'This credit card is already registered';

export const userDeactivated = 'this user is no longer active in the system';

export const invalidCardDate = 'Invalid expire date. Must be a future ';

export const cardNotFound = 'Card not found';

export const cardNumberAndCardIdMismatch ='The card number does not match the Card id provided in the url';

export const cardBlocked = 'Forbidden! Card Block';

export const fileTooBig = `The max avatar size is ${maxAvatarSize/1024} MegaByte(s)`;

export const avatarImageCanotBeEmpty = 'Avatar image missing';

export const avatarNotFound = 'Avatar image not found';

export const wrongPassword = 'Wrong password or email.';