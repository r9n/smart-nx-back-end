import { EntityRepository, Repository } from "typeorm";
import { User } from "../user/user.entity";
import { Card } from "./card.entity";
import * as bcrypt from 'bcrypt';


@EntityRepository(Card)
export class CardRepository extends Repository<Card>{

 
    
async findByNumber(encryptedNumber:string){
    return this.findOne({number:encryptedNumber});
}    

async findById(id:string):Promise<Card>{
    return this.findOne(id);
}
}