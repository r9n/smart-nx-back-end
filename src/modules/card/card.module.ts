import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from '../user/user.module';
import { CardRepository } from './card.repository';
import { CardService } from './card.service';

@Module({
    imports:[TypeOrmModule.forFeature([CardRepository,CardRepository]),UserModule],
    providers:[CardService],
    exports:[TypeOrmModule,CardService]
})
export class CardModule {}
