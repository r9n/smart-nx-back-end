import {
  MAX_CARD_HOLDER_NAME_LENGHT,
  MAX_CARD_SECURE_CODE_LENGHT,
} from 'src/config/constraints';
import { CardBrands } from 'src/enums/card-brands.enum';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../base.entity';
import { User } from '../user/user.entity';
import { CreateCardDto } from './dto/create-card.dto';

@Entity()
export class Card extends BaseEntity {
  @Column({ type: 'varchar', length: MAX_CARD_HOLDER_NAME_LENGHT })
  holder: string;

  @Column({ type: 'varchar',select:false})
  number: string;

  @Column({ type: 'date' })
  expireDate: Date;

  @Column({ type: 'varchar', length: MAX_CARD_SECURE_CODE_LENGHT })
  secureCode: string;

  @Column({ type: 'boolean' })
  isBlocked: boolean;

  @Column({ enum: CardBrands })
  brand: CardBrands;

  @ManyToOne((type) => User, (user) => user.card)
  user: User;

  constructor(partial:Partial<Card>){
    super();
    Object.assign(this,partial);
  }
  fromDto(dto:CreateCardDto):void{
    this.brand = dto.brand ? dto.brand:this.brand;
    this.holder = dto.holder ? dto.holder:this.holder;
    this.secureCode = dto.secureCode ? dto.secureCode:this.secureCode;
    this.expireDate = dto.expireDate ? dto.expireDate:this.expireDate;
  }
}
