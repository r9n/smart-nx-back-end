import { Body, Controller, Param, Patch, Post } from '@nestjs/common';
import { ApiForbiddenResponse, ApiCreatedResponse, ApiNotFoundResponse } from '@nestjs/swagger';
import { cardBlocked, cardNotFound, cardNumberAndCardIdMismatch, invalidCardDate, userDeactivated, userNotFound } from '../messages/error.messages';
import { Card } from './card.entity';
import { CardService } from './card.service';
import { CreateCardDto } from './dto/create-card.dto';
import { EditCardDto } from './dto/edit-card.dto';

@Controller('card')
export class CardController {

    constructor(private cardService:CardService){}

    @Post()
    @ApiForbiddenResponse({
        description: `${userDeactivated},
        ${invalidCardDate}
        `
    })
    @ApiNotFoundResponse({description:userNotFound})
    @ApiCreatedResponse({type:Card})
    async create(@Body() dto:CreateCardDto):Promise<Card>{        
     return this.cardService.createOne(dto);  
    }
    
    @Post('/edit/:id')
    @ApiForbiddenResponse({
        description: `${userDeactivated},
        ${invalidCardDate}, ${cardNumberAndCardIdMismatch},${cardBlocked}
        `
    })
    @ApiNotFoundResponse({description:`${userNotFound}, ${cardNotFound}`,})
    @ApiCreatedResponse({type:Card})
    async edit(@Body() dto:EditCardDto,@Param('id') cardId:string):Promise<Card>{        
     return this.cardService.edit(dto,cardId);  
    }

    @Patch('/block/:id')
    @ApiForbiddenResponse({
        description: invalidCardDate})
    @ApiNotFoundResponse({description:cardNotFound,})
    @ApiCreatedResponse({type:Card})
    async block(@Param('id')cardId:string):Promise<Card>{        
     return this.cardService.block(cardId);  
    }

    @Patch('/unblock/:id')
    @ApiForbiddenResponse({
        description: `${userDeactivated},
        ${invalidCardDate}
        `
    })
    @ApiNotFoundResponse({description:cardNotFound,})
    @ApiCreatedResponse({type:Card})
    async unBlock(@Param('id')cardId:string):Promise<Card>{        
     return this.cardService.unBlock(cardId);  
    }


}
