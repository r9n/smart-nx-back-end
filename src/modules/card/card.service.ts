import { ForbiddenException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { CardRepository } from './card.repository';
import { CreateCardDto } from './dto/create-card.dto';
import { cardAlreadiRegistered, cardBlocked, cardNotFound, cardNumberAndCardIdMismatch, invalidCardDate, userDeactivated, userNotFound } from "../messages/error.messages";
import { UserService } from '../user/user.service';
import { Card } from './card.entity';
import * as bcrypt from 'bcrypt';
import { EditCardDto } from './dto/edit-card.dto';
@Injectable()
export class CardService {
    constructor(private cardRepository:CardRepository,private userService:UserService){}

async createOne(dto:CreateCardDto):Promise<Card>{
 const user = await this.userService.findUserById(dto.userId);

 if(!user.isActive){
     throw new ForbiddenException(userDeactivated)
 }
 const encryptedNumber  =  await bcrypt.hash(dto.number,user.salt)

 const existingCard = await this.cardRepository.findByNumber(encryptedNumber);

 if(existingCard){
     throw new ForbiddenException(cardAlreadiRegistered);
 }

 if(new Date(dto.expireDate) < new Date()){
     throw new ForbiddenException(invalidCardDate);
 }

 const newCard = new Card({});
 newCard.fromDto(dto);
 newCard.number = encryptedNumber;
 newCard.isBlocked = false;
 newCard.user = user;

 return this.cardRepository.save(newCard)
 .then((user)=>{
    return new Card({ brand: newCard.brand,
     holder: newCard.holder,
     secureCode: newCard.secureCode,
     expireDate:newCard.expireDate,
     isBlocked: false,
     id: newCard.id,
    });
     
     
 })
 .catch((error)=>{
     throw new InternalServerErrorException(error.message);
 })
    
}


async edit(dto:EditCardDto,cardId:string): Promise<Card>{
    const user = await this.userService.findUserById(dto.userId);

    if(!user.isActive){
        throw new ForbiddenException(userDeactivated)
    }
    const encryptedNumber  =  await bcrypt.hash(dto.number,user.salt)
   
    const existingCard = await this.cardRepository.findByNumber(encryptedNumber);
   
    if(!existingCard){
        throw new ForbiddenException(cardNotFound);
    }
   
    if(existingCard.id !== cardId){
        throw new ForbiddenException(cardNumberAndCardIdMismatch);
    }

    if(existingCard.isBlocked){
        throw new ForbiddenException(cardBlocked);
    }
    
    if(new Date(dto.expireDate) < new Date()){
        throw new ForbiddenException(invalidCardDate);
    }
   
    existingCard.fromDto(dto);
    existingCard.number = encryptedNumber;
   
    return this.cardRepository.save(existingCard).catch((error)=>{
        throw new InternalServerErrorException(error.message);
    })
}

async block(cardId:string):Promise<Card>{
    const existingCard = await this.cardRepository.findById(cardId);

    if(!existingCard){
        throw new ForbiddenException(cardNotFound);
    }
    existingCard.isBlocked = true;
    return this.cardRepository.save(existingCard).catch((error)=>{
        throw new InternalServerErrorException(error.message);
    });
}

async unBlock(cardId:string):Promise<Card>{
    const existingCard = await this.cardRepository.findById(cardId);

    if(!existingCard){
        throw new ForbiddenException(cardNotFound);
    }
    existingCard.isBlocked = false;
    return this.cardRepository.save(existingCard).catch((error)=>{
        throw new InternalServerErrorException(error.message);
    })
}
}
