import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, isUUID, MaxLength, MinLength } from 'class-validator';
import {
    MAX_CARD_HOLDER_NAME_LENGHT,
    MAX_CARD_NUMBER_LENGHT,
    MAX_CARD_SECURE_CODE_LENGHT,
    MIN_CARD_HOLDER_NAME_LENGHT,
    MIN_CARD_NUMBER_LENGHT,
    MIN_CARD_SECURE_CODE_LENGHT,
  } from 'src/config/constraints';
  import { CardBrands } from 'src/enums/card-brands.enum';

  
  export class EditCardDto {
    @ApiProperty({type:String,minLength:MIN_CARD_HOLDER_NAME_LENGHT,
        maxLength:MAX_CARD_HOLDER_NAME_LENGHT,required:true})
    @MinLength(MIN_CARD_HOLDER_NAME_LENGHT)
    @MaxLength(MAX_CARD_HOLDER_NAME_LENGHT)
    @IsNotEmpty()
    holder: string;
  
    @ApiProperty({type:String,minLength:MIN_CARD_NUMBER_LENGHT,
        maxLength:MAX_CARD_NUMBER_LENGHT,required:true})
    @MinLength(MIN_CARD_NUMBER_LENGHT)
    @MaxLength(MAX_CARD_NUMBER_LENGHT)
    @IsNotEmpty()
    number: string;
  
    @ApiProperty({type:Date,required:true})
    @IsNotEmpty()
    expireDate: Date;
  
    @ApiProperty({type:String,minLength:MIN_CARD_SECURE_CODE_LENGHT,
        maxLength:MAX_CARD_SECURE_CODE_LENGHT,required:true})
    @MinLength(MIN_CARD_SECURE_CODE_LENGHT)
    @MaxLength(MAX_CARD_SECURE_CODE_LENGHT)
    @IsNotEmpty()
    secureCode: string;

    @ApiProperty({type:CardBrands,enum:CardBrands,required:true})
    @IsNotEmpty()
    @IsEnum(CardBrands)
    brand: CardBrands;

    @ApiProperty({type:'uuid',required:true})
    @IsNotEmpty()
    userId: string;

  }
  