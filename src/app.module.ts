import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { CountryController } from './modules/address/country/country.controller';
import { CityController } from './modules/address/city/city.controller';
import { StateController } from './modules/address/state/state.controller';
import { StateService } from './modules/address/state/state.service';
import { CountryService } from './modules/address/country/country.service';
import { CityService } from './modules/address/city/city.service';
import { CityModule } from './modules/address/city/city.module';
import { CountryModule } from './modules/address/country/country.module';
import { StateModule } from './modules/address/state/state.module';
import { AddressModule } from './modules/address/address.module';
import { CardController } from './modules/card/card.controller';
import { CardModule } from './modules/card/card.module';
import { CardService } from './modules/card/card.service';
import { UserController } from './modules/user/user.controller';
import { UserModule } from './modules/user/user.module';
import { UserService } from './modules/user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
import { AuthController } from './modules/auth/auth.controller';
import { AuthService } from './modules/auth/auth.service';
import { AuthModule } from './modules/auth/auth.module';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    AuthModule,
    CardModule,
    UserModule,
    AddressModule,
    CityModule,
    CountryModule,
    StateModule,
  ],
  controllers: [
    AppController,
    UserController,
    CardController,
    CountryController,
    CityController,
    StateController,
    AuthController
  ],
  providers: [
    AppService,
    CardService,
    UserService,
    StateService,
    CountryService,
    CityService,
    AuthService
  ],
})
export class AppModule {}
