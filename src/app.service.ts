import { Injectable } from '@nestjs/common';
import { API_VERION } from './config/constraints';

@Injectable()
export class AppService {
  getHello(): string {
    return `SmartNx Api Version: ${API_VERION}`;
  }
}
