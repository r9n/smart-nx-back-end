import {MigrationInterface, QueryRunner} from "typeorm";

export class addingCard1617803214442 implements MigrationInterface {
    name = 'addingCard1617803214442'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "card" DROP COLUMN "number"`);
        await queryRunner.query(`ALTER TABLE "card" ADD "number" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "card" DROP COLUMN "number"`);
        await queryRunner.query(`ALTER TABLE "card" ADD "number" character varying(60) NOT NULL`);
    }

}
