import {MigrationInterface, QueryRunner} from "typeorm";

export class changeDatabaseStruct1617906703801 implements MigrationInterface {
    name = 'changeDatabaseStruct1617906703801'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "address" DROP CONSTRAINT "FK_cc9b2ed4ab9debaf6cb78bd0330"`);
        await queryRunner.query(`ALTER TABLE "address" DROP COLUMN "usersId"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "addressId" uuid`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_217ba147c5de6c107f2fa7fa271" FOREIGN KEY ("addressId") REFERENCES "address"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_217ba147c5de6c107f2fa7fa271"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "addressId"`);
        await queryRunner.query(`ALTER TABLE "address" ADD "usersId" uuid`);
        await queryRunner.query(`ALTER TABLE "address" ADD CONSTRAINT "FK_cc9b2ed4ab9debaf6cb78bd0330" FOREIGN KEY ("usersId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
