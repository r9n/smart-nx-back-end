import { MigrationInterface, QueryRunner } from 'typeorm';

export class changeAddressDatabaseStructure1617732602095
  implements MigrationInterface {
  name = 'changeAddressDatabaseStructure1617732602095';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "address" DROP CONSTRAINT "FK_d87215343c3a3a67e6a0b7f3ea9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "address" DROP CONSTRAINT "FK_1220f0989240d6c0d580a61bd30"`,
    );
    await queryRunner.query(`ALTER TABLE "address" DROP COLUMN "countryId"`);
    await queryRunner.query(`ALTER TABLE "address" DROP COLUMN "stateId"`);
    await queryRunner.query(`ALTER TABLE "state" ADD "countryId" uuid`);
    await queryRunner.query(`ALTER TABLE "city" ADD "stateId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "state" ADD CONSTRAINT "FK_e81c86ceadca8731f5fca8e06f5" FOREIGN KEY ("countryId") REFERENCES "country"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "city" ADD CONSTRAINT "FK_e99de556ee56afe72154f3ed04a" FOREIGN KEY ("stateId") REFERENCES "state"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "city" DROP CONSTRAINT "FK_e99de556ee56afe72154f3ed04a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "state" DROP CONSTRAINT "FK_e81c86ceadca8731f5fca8e06f5"`,
    );
    await queryRunner.query(`ALTER TABLE "city" DROP COLUMN "stateId"`);
    await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "countryId"`);
    await queryRunner.query(`ALTER TABLE "address" ADD "stateId" uuid`);
    await queryRunner.query(`ALTER TABLE "address" ADD "countryId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "address" ADD CONSTRAINT "FK_1220f0989240d6c0d580a61bd30" FOREIGN KEY ("stateId") REFERENCES "state"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "address" ADD CONSTRAINT "FK_d87215343c3a3a67e6a0b7f3ea9" FOREIGN KEY ("countryId") REFERENCES "country"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
