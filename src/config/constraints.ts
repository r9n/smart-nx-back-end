//Api
export const API_VERION = '1.0.0';
export const API_TITLE = 'SmartNx Api';
export const API_DESCRIPTION =
  'Uma api simples para gerenciamento de usuários e cartões';

//User

export const MAX_USER_NAME_LENGHT = 60;
export const MIN_USER_NAME_LENGHT = 3;

export const MAX_USER_EMAIL_LENGHT = 40;
export const MIN_USER_EMAIL_LENGHT = 7; //s@d.com => mínimo 7 caracteres

export const MAX_USER_PASSWORD= 8;
export const MIN_USER_PASSWORD = 3;

export const Kbyte = 1; 
export const MegaByte = 1024 * Kbyte;
export const maxAvatarSize  = 10*MegaByte;

export const avatarsFolder = './avatars';

export const tempAvatarsFolder = './tempAvatars';

//Card
export const MAX_CARD_HOLDER_NAME_LENGHT = 60;
export const MIN_CARD_HOLDER_NAME_LENGHT = 3;

export const MAX_CARD_NUMBER_LENGHT = 40;
export const MIN_CARD_NUMBER_LENGHT = 8;

export const MAX_CARD_BRAND_NAME_LENGHT = 16;
export const MIN_CARD_BRAND_NAME_LENGHT = 3;

export const MAX_CARD_SECURE_CODE_LENGHT = 6;
export const MIN_CARD_SECURE_CODE_LENGHT = 4;


///City
export const MAX_CITY_NAME_LENGHT = 128;
export const MIN_CITY_NAME_LENGHT = 2;

///State
export const MAX_STATE_LENGHT = 128;
export const MIN_STATE_LENGHT = 2;

//Country
export const MAX_COUNTRY_NAME_LENGHT = 64;
export const MIN_COUNTRY_NAME_LENGHT = 2;

//Address
export const MAX_ADDRESS_STREET_LENGHT = 64;
export const MIN_ADDRESS_STREE_LENGHT = 2;

export const MAX_ADDRESS_POSTALCODE_LENGHT = 64;
export const MIN_ADDRESS_POSTALCODE_LENGHT = 2;
