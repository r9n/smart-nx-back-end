import { ConnectionOptions } from 'typeorm';

function getMigrationDirectory() {
  const directory =
    process.env.NODE_ENV === 'migration' ? 'src' : `${__dirname}`;
  return `${directory}/migrations/**/*{.ts,.js}`;
}

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,

  entities: [`${__dirname}/../**/*.entity.{ts,js}`],

  synchronize: false,
  dropSchema: false,

  migrationsRun: false,
  logging: ['warn', 'error'],
  logger: 'file',

  migrations: [getMigrationDirectory()],
  cli: {
    migrationsDir: 'src/migrations',
  },
};

export = connectionOptions;
