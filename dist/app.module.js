"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const country_controller_1 = require("./modules/address/country/country.controller");
const city_controller_1 = require("./modules/address/city/city.controller");
const state_controller_1 = require("./modules/address/state/state.controller");
const state_service_1 = require("./modules/address/state/state.service");
const country_service_1 = require("./modules/address/country/country.service");
const city_service_1 = require("./modules/address/city/city.service");
const city_module_1 = require("./modules/address/city/city.module");
const country_module_1 = require("./modules/address/country/country.module");
const state_module_1 = require("./modules/address/state/state.module");
const address_module_1 = require("./modules/address/address.module");
const card_controller_1 = require("./modules/card/card.controller");
const card_module_1 = require("./modules/card/card.module");
const card_service_1 = require("./modules/card/card.service");
const user_controller_1 = require("./modules/user/user.controller");
const user_module_1 = require("./modules/user/user.module");
const user_service_1 = require("./modules/user/user.service");
const typeorm_1 = require("@nestjs/typeorm");
const nestjs_config_1 = require("nestjs-config");
const path = require("path");
const auth_controller_1 = require("./modules/auth/auth.controller");
const auth_service_1 = require("./modules/auth/auth.service");
const auth_module_1 = require("./modules/auth/auth.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            nestjs_config_1.ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
            typeorm_1.TypeOrmModule.forRootAsync({
                useFactory: (config) => config.get('database'),
                inject: [nestjs_config_1.ConfigService],
            }),
            auth_module_1.AuthModule,
            card_module_1.CardModule,
            user_module_1.UserModule,
            address_module_1.AddressModule,
            city_module_1.CityModule,
            country_module_1.CountryModule,
            state_module_1.StateModule,
        ],
        controllers: [
            app_controller_1.AppController,
            user_controller_1.UserController,
            card_controller_1.CardController,
            country_controller_1.CountryController,
            city_controller_1.CityController,
            state_controller_1.StateController,
            auth_controller_1.AuthController
        ],
        providers: [
            app_service_1.AppService,
            card_service_1.CardService,
            user_service_1.UserService,
            state_service_1.StateService,
            country_service_1.CountryService,
            city_service_1.CityService,
            auth_service_1.AuthService
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map