"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.wrongPassword = exports.avatarNotFound = exports.avatarImageCanotBeEmpty = exports.fileTooBig = exports.cardBlocked = exports.cardNumberAndCardIdMismatch = exports.cardNotFound = exports.invalidCardDate = exports.userDeactivated = exports.cardAlreadiRegistered = exports.userAlreadyActivated = exports.userAlreadyDeactivated = exports.userNotFound = exports.emailAlreadyRegistered = void 0;
const constraints_1 = require("../../config/constraints");
exports.emailAlreadyRegistered = 'This email is already registered in our database';
exports.userNotFound = 'This user is not registered on our systems';
exports.userAlreadyDeactivated = 'This user is already deactivated';
exports.userAlreadyActivated = 'This user is already activated';
exports.cardAlreadiRegistered = 'This credit card is already registered';
exports.userDeactivated = 'this user is no longer active in the system';
exports.invalidCardDate = 'Invalid expire date. Must be a future ';
exports.cardNotFound = 'Card not found';
exports.cardNumberAndCardIdMismatch = 'The card number does not match the Card id provided in the url';
exports.cardBlocked = 'Forbidden! Card Block';
exports.fileTooBig = `The max avatar size is ${constraints_1.maxAvatarSize / 1024} MegaByte(s)`;
exports.avatarImageCanotBeEmpty = 'Avatar image missing';
exports.avatarNotFound = 'Avatar image not found';
exports.wrongPassword = 'Wrong password or email.';
//# sourceMappingURL=error.messages.js.map