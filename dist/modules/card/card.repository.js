"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardRepository = void 0;
const typeorm_1 = require("typeorm");
const card_entity_1 = require("./card.entity");
let CardRepository = class CardRepository extends typeorm_1.Repository {
    async findByNumber(encryptedNumber) {
        return this.findOne({ number: encryptedNumber });
    }
    async findById(id) {
        return this.findOne(id);
    }
};
CardRepository = __decorate([
    typeorm_1.EntityRepository(card_entity_1.Card)
], CardRepository);
exports.CardRepository = CardRepository;
//# sourceMappingURL=card.repository.js.map