import { CardRepository } from './card.repository';
import { CreateCardDto } from './dto/create-card.dto';
import { UserService } from '../user/user.service';
import { Card } from './card.entity';
import { EditCardDto } from './dto/edit-card.dto';
export declare class CardService {
    private cardRepository;
    private userService;
    constructor(cardRepository: CardRepository, userService: UserService);
    createOne(dto: CreateCardDto): Promise<Card>;
    edit(dto: EditCardDto, cardId: string): Promise<Card>;
    block(cardId: string): Promise<Card>;
    unBlock(cardId: string): Promise<Card>;
}
