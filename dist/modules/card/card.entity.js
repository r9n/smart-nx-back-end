"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Card = void 0;
const constraints_1 = require("../../config/constraints");
const card_brands_enum_1 = require("../../enums/card-brands.enum");
const typeorm_1 = require("typeorm");
const base_entity_1 = require("../base.entity");
const user_entity_1 = require("../user/user.entity");
let Card = class Card extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
    fromDto(dto) {
        this.brand = dto.brand ? dto.brand : this.brand;
        this.holder = dto.holder ? dto.holder : this.holder;
        this.secureCode = dto.secureCode ? dto.secureCode : this.secureCode;
        this.expireDate = dto.expireDate ? dto.expireDate : this.expireDate;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_CARD_HOLDER_NAME_LENGHT }),
    __metadata("design:type", String)
], Card.prototype, "holder", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', select: false }),
    __metadata("design:type", String)
], Card.prototype, "number", void 0);
__decorate([
    typeorm_1.Column({ type: 'date' }),
    __metadata("design:type", Date)
], Card.prototype, "expireDate", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_CARD_SECURE_CODE_LENGHT }),
    __metadata("design:type", String)
], Card.prototype, "secureCode", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean' }),
    __metadata("design:type", Boolean)
], Card.prototype, "isBlocked", void 0);
__decorate([
    typeorm_1.Column({ enum: card_brands_enum_1.CardBrands }),
    __metadata("design:type", String)
], Card.prototype, "brand", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => user_entity_1.User, (user) => user.card),
    __metadata("design:type", user_entity_1.User)
], Card.prototype, "user", void 0);
Card = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Card);
exports.Card = Card;
//# sourceMappingURL=card.entity.js.map