import { Card } from './card.entity';
import { CardService } from './card.service';
import { CreateCardDto } from './dto/create-card.dto';
import { EditCardDto } from './dto/edit-card.dto';
export declare class CardController {
    private cardService;
    constructor(cardService: CardService);
    create(dto: CreateCardDto): Promise<Card>;
    edit(dto: EditCardDto, cardId: string): Promise<Card>;
    block(cardId: string): Promise<Card>;
    unBlock(cardId: string): Promise<Card>;
}
