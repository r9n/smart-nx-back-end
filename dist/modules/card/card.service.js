"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardService = void 0;
const common_1 = require("@nestjs/common");
const card_repository_1 = require("./card.repository");
const error_messages_1 = require("../messages/error.messages");
const user_service_1 = require("../user/user.service");
const card_entity_1 = require("./card.entity");
const bcrypt = require("bcrypt");
let CardService = class CardService {
    constructor(cardRepository, userService) {
        this.cardRepository = cardRepository;
        this.userService = userService;
    }
    async createOne(dto) {
        const user = await this.userService.findUserById(dto.userId);
        if (!user.isActive) {
            throw new common_1.ForbiddenException(error_messages_1.userDeactivated);
        }
        const encryptedNumber = await bcrypt.hash(dto.number, user.salt);
        const existingCard = await this.cardRepository.findByNumber(encryptedNumber);
        if (existingCard) {
            throw new common_1.ForbiddenException(error_messages_1.cardAlreadiRegistered);
        }
        if (new Date(dto.expireDate) < new Date()) {
            throw new common_1.ForbiddenException(error_messages_1.invalidCardDate);
        }
        const newCard = new card_entity_1.Card({});
        newCard.fromDto(dto);
        newCard.number = encryptedNumber;
        newCard.isBlocked = false;
        newCard.user = user;
        return this.cardRepository.save(newCard)
            .then((user) => {
            return new card_entity_1.Card({ brand: newCard.brand,
                holder: newCard.holder,
                secureCode: newCard.secureCode,
                expireDate: newCard.expireDate,
                isBlocked: false,
                id: newCard.id,
            });
        })
            .catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async edit(dto, cardId) {
        const user = await this.userService.findUserById(dto.userId);
        if (!user.isActive) {
            throw new common_1.ForbiddenException(error_messages_1.userDeactivated);
        }
        const encryptedNumber = await bcrypt.hash(dto.number, user.salt);
        const existingCard = await this.cardRepository.findByNumber(encryptedNumber);
        if (!existingCard) {
            throw new common_1.ForbiddenException(error_messages_1.cardNotFound);
        }
        if (existingCard.id !== cardId) {
            throw new common_1.ForbiddenException(error_messages_1.cardNumberAndCardIdMismatch);
        }
        if (existingCard.isBlocked) {
            throw new common_1.ForbiddenException(error_messages_1.cardBlocked);
        }
        if (new Date(dto.expireDate) < new Date()) {
            throw new common_1.ForbiddenException(error_messages_1.invalidCardDate);
        }
        existingCard.fromDto(dto);
        existingCard.number = encryptedNumber;
        return this.cardRepository.save(existingCard).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async block(cardId) {
        const existingCard = await this.cardRepository.findById(cardId);
        if (!existingCard) {
            throw new common_1.ForbiddenException(error_messages_1.cardNotFound);
        }
        existingCard.isBlocked = true;
        return this.cardRepository.save(existingCard).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async unBlock(cardId) {
        const existingCard = await this.cardRepository.findById(cardId);
        if (!existingCard) {
            throw new common_1.ForbiddenException(error_messages_1.cardNotFound);
        }
        existingCard.isBlocked = false;
        return this.cardRepository.save(existingCard).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
};
CardService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [card_repository_1.CardRepository, user_service_1.UserService])
], CardService);
exports.CardService = CardService;
//# sourceMappingURL=card.service.js.map