"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const error_messages_1 = require("../messages/error.messages");
const card_entity_1 = require("./card.entity");
const card_service_1 = require("./card.service");
const create_card_dto_1 = require("./dto/create-card.dto");
const edit_card_dto_1 = require("./dto/edit-card.dto");
let CardController = class CardController {
    constructor(cardService) {
        this.cardService = cardService;
    }
    async create(dto) {
        return this.cardService.createOne(dto);
    }
    async edit(dto, cardId) {
        return this.cardService.edit(dto, cardId);
    }
    async block(cardId) {
        return this.cardService.block(cardId);
    }
    async unBlock(cardId) {
        return this.cardService.unBlock(cardId);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiForbiddenResponse({
        description: `${error_messages_1.userDeactivated},
        ${error_messages_1.invalidCardDate}
        `
    }),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.userNotFound }),
    swagger_1.ApiCreatedResponse({ type: card_entity_1.Card }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_card_dto_1.CreateCardDto]),
    __metadata("design:returntype", Promise)
], CardController.prototype, "create", null);
__decorate([
    common_1.Post('/edit/:id'),
    swagger_1.ApiForbiddenResponse({
        description: `${error_messages_1.userDeactivated},
        ${error_messages_1.invalidCardDate}, ${error_messages_1.cardNumberAndCardIdMismatch},${error_messages_1.cardBlocked}
        `
    }),
    swagger_1.ApiNotFoundResponse({ description: `${error_messages_1.userNotFound}, ${error_messages_1.cardNotFound}`, }),
    swagger_1.ApiCreatedResponse({ type: card_entity_1.Card }),
    __param(0, common_1.Body()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [edit_card_dto_1.EditCardDto, String]),
    __metadata("design:returntype", Promise)
], CardController.prototype, "edit", null);
__decorate([
    common_1.Patch('/block/:id'),
    swagger_1.ApiForbiddenResponse({
        description: error_messages_1.invalidCardDate
    }),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.cardNotFound, }),
    swagger_1.ApiCreatedResponse({ type: card_entity_1.Card }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CardController.prototype, "block", null);
__decorate([
    common_1.Patch('/unblock/:id'),
    swagger_1.ApiForbiddenResponse({
        description: `${error_messages_1.userDeactivated},
        ${error_messages_1.invalidCardDate}
        `
    }),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.cardNotFound, }),
    swagger_1.ApiCreatedResponse({ type: card_entity_1.Card }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CardController.prototype, "unBlock", null);
CardController = __decorate([
    common_1.Controller('card'),
    __metadata("design:paramtypes", [card_service_1.CardService])
], CardController);
exports.CardController = CardController;
//# sourceMappingURL=card.controller.js.map