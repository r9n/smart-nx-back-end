import { CardBrands } from 'src/enums/card-brands.enum';
import { BaseEntity } from '../base.entity';
import { User } from '../user/user.entity';
import { CreateCardDto } from './dto/create-card.dto';
export declare class Card extends BaseEntity {
    holder: string;
    number: string;
    expireDate: Date;
    secureCode: string;
    isBlocked: boolean;
    brand: CardBrands;
    user: User;
    constructor(partial: Partial<Card>);
    fromDto(dto: CreateCardDto): void;
}
