import { CardBrands } from 'src/enums/card-brands.enum';
export declare class EditCardDto {
    holder: string;
    number: string;
    expireDate: Date;
    secureCode: string;
    brand: CardBrands;
    userId: string;
}
