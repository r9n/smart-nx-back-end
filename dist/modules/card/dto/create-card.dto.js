"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateCardDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const constraints_1 = require("../../../config/constraints");
const card_brands_enum_1 = require("../../../enums/card-brands.enum");
class CreateCardDto {
}
__decorate([
    swagger_1.ApiProperty({ type: String, minLength: constraints_1.MIN_CARD_HOLDER_NAME_LENGHT,
        maxLength: constraints_1.MAX_CARD_HOLDER_NAME_LENGHT, required: true }),
    class_validator_1.MinLength(constraints_1.MIN_CARD_HOLDER_NAME_LENGHT),
    class_validator_1.MaxLength(constraints_1.MAX_CARD_HOLDER_NAME_LENGHT),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CreateCardDto.prototype, "holder", void 0);
__decorate([
    swagger_1.ApiProperty({ type: String, minLength: constraints_1.MIN_CARD_NUMBER_LENGHT,
        maxLength: constraints_1.MAX_CARD_NUMBER_LENGHT, required: true }),
    class_validator_1.MinLength(constraints_1.MIN_CARD_NUMBER_LENGHT),
    class_validator_1.MaxLength(constraints_1.MAX_CARD_NUMBER_LENGHT),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CreateCardDto.prototype, "number", void 0);
__decorate([
    swagger_1.ApiProperty({ type: Date, required: true }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Date)
], CreateCardDto.prototype, "expireDate", void 0);
__decorate([
    swagger_1.ApiProperty({ type: String, minLength: constraints_1.MIN_CARD_SECURE_CODE_LENGHT,
        maxLength: constraints_1.MAX_CARD_SECURE_CODE_LENGHT, required: true }),
    class_validator_1.MinLength(constraints_1.MIN_CARD_SECURE_CODE_LENGHT),
    class_validator_1.MaxLength(constraints_1.MAX_CARD_SECURE_CODE_LENGHT),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CreateCardDto.prototype, "secureCode", void 0);
__decorate([
    swagger_1.ApiProperty({ type: card_brands_enum_1.CardBrands, enum: card_brands_enum_1.CardBrands, required: true }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEnum(card_brands_enum_1.CardBrands),
    __metadata("design:type", String)
], CreateCardDto.prototype, "brand", void 0);
__decorate([
    swagger_1.ApiProperty({ type: 'uuid', required: true }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CreateCardDto.prototype, "userId", void 0);
exports.CreateCardDto = CreateCardDto;
//# sourceMappingURL=create-card.dto.js.map