import { CardBrands } from 'src/enums/card-brands.enum';
export declare class CreateCardDto {
    holder: string;
    number: string;
    expireDate: Date;
    secureCode: string;
    brand: CardBrands;
    userId: string;
}
