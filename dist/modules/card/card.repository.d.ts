import { Repository } from "typeorm";
import { Card } from "./card.entity";
export declare class CardRepository extends Repository<Card> {
    findByNumber(encryptedNumber: string): Promise<Card>;
    findById(id: string): Promise<Card>;
}
