"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const error_messages_1 = require("../messages/error.messages");
const user_entity_1 = require("../user/user.entity");
const user_service_1 = require("../user/user.service");
const signin_response_dto_1 = require("./dto/signin-response.dto");
const bcrypt = require("bcrypt");
const roles_enum_1 = require("../../enums/roles.enum");
let AuthService = class AuthService {
    constructor(userService) {
        this.userService = userService;
    }
    async signup(dto) {
        const newUser = new user_entity_1.User(dto);
        const hasAccount = await this.userService.findUserByEmail(dto.email);
        if (hasAccount) {
            throw new common_1.ForbiddenException(error_messages_1.emailAlreadyRegistered);
        }
        newUser.salt = await bcrypt.genSalt();
        newUser.passwordHash = await this.createHasPassword(dto.password, newUser.salt);
        newUser.isActive = true;
        newUser.role = roles_enum_1.Roles.USER;
        return this.userService.save(newUser).then((user) => {
            user.passwordHash = '';
            return user;
        })
            .catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async signin(dto) {
        const user = await this.userService.findUserByEmail(dto.email);
        if (!user) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        if (await this.userService.validateUserPassword(dto) === true) {
            user.salt = '';
            user.passwordHash = '';
            const signinReponseDto = new signin_response_dto_1.SigninResponseDto(user);
            return signinReponseDto;
        }
        throw new common_1.ForbiddenException(error_messages_1.wrongPassword);
    }
    async createHasPassword(password, salt) {
        return bcrypt.hash(password, salt);
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [user_service_1.UserService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map