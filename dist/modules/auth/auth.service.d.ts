import { User } from "../user/user.entity";
import { UserService } from "../user/user.service";
import { SigninResponseDto } from "./dto/signin-response.dto";
import { SigninDto } from "./dto/signin.dto";
import { CreateUserDto } from "./dto/signup.dto";
export declare class AuthService {
    private userService;
    constructor(userService: UserService);
    signup(dto: CreateUserDto): Promise<User>;
    signin(dto: SigninDto): Promise<SigninResponseDto>;
    private createHasPassword;
}
