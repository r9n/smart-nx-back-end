import { User } from "../user/user.entity";
import { AuthService } from "./auth.service";
import { SigninResponseDto } from "./dto/signin-response.dto";
import { SigninDto } from "./dto/signin.dto";
import { CreateUserDto } from "./dto/signup.dto";
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signup(dto: CreateUserDto): Promise<User>;
    singin(dto: SigninDto): Promise<SigninResponseDto>;
}
