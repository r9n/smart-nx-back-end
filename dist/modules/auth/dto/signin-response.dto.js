"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SigninResponseDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const roles_enum_1 = require("../../../enums/roles.enum");
const address_entity_1 = require("../../address/address.entity");
const card_entity_1 = require("../../card/card.entity");
const typeorm_1 = require("typeorm");
class SigninResponseDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
}
__decorate([
    swagger_1.ApiProperty({ type: String, description: 'UUID' }),
    __metadata("design:type", String)
], SigninResponseDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ type: String }),
    __metadata("design:type", String)
], SigninResponseDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ type: Boolean }),
    __metadata("design:type", Boolean)
], SigninResponseDto.prototype, "isActive", void 0);
__decorate([
    swagger_1.ApiProperty({ type: String }),
    __metadata("design:type", Array)
], SigninResponseDto.prototype, "card", void 0);
__decorate([
    swagger_1.ApiProperty({ type: address_entity_1.Address }),
    __metadata("design:type", address_entity_1.Address)
], SigninResponseDto.prototype, "address", void 0);
__decorate([
    typeorm_1.Column({ type: 'enum', enum: roles_enum_1.Roles }),
    __metadata("design:type", String)
], SigninResponseDto.prototype, "role", void 0);
exports.SigninResponseDto = SigninResponseDto;
//# sourceMappingURL=signin-response.dto.js.map