import { Roles } from "src/enums/roles.enum";
import { Address } from "src/modules/address/address.entity";
import { Card } from "src/modules/card/card.entity";
export declare class SigninResponseDto {
    id: string;
    email: string;
    isActive: boolean;
    card: Card[];
    address: Address;
    role: Roles;
    constructor(partial: Partial<SigninResponseDto>);
}
