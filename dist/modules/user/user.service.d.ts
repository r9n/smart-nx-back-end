/// <reference types="multer" />
import { SigninDto } from "../auth/dto/signin.dto";
import { EditUserDto } from "./dto/edit-user.dto";
import { User } from "./user.entity";
import { UserRepository } from "./user.repository";
export declare class UserService {
    private userRepository;
    constructor(userRepository: UserRepository);
    save(user: User): Promise<User>;
    findUserByEmail(email: string): Promise<User>;
    findUserById(id: string): Promise<User>;
    validateUserPassword(dto: SigninDto): Promise<Boolean>;
    editUser(dto: EditUserDto, userId: string): Promise<User>;
    reactivateUser(userId: string): Promise<User>;
    deactivate(userId: string): Promise<User>;
    saveAvatar(avatarFile: Express.Multer.File, userId: string): Promise<void>;
    getAvatarPath(userId: string): Promise<string>;
}
