/// <reference types="multer" />
import { EditUserDto } from './dto/edit-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    editUser(dto: EditUserDto, userId: string): Promise<User>;
    deactivate(userId: string): Promise<User>;
    reactivate(userId: string): Promise<User>;
    uploadAvatar(avatarFile: Express.Multer.File, userId: string): Promise<void>;
    getAvatar(userId: string, res: any): Promise<void>;
}
