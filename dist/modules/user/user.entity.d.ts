import { Address } from '../address/address.entity';
import { BaseEntity } from '../base.entity';
import { Card } from '../card/card.entity';
import { EditUserDto } from './dto/edit-user.dto';
import { Roles } from 'src/enums/roles.enum';
export declare class User extends BaseEntity {
    name: string;
    email: string;
    isActive: boolean;
    card: Card[];
    address: Address;
    passwordHash: string;
    salt: string;
    role: Roles;
    constructor(partial: Partial<User>);
    validationPassword(password: string): Promise<boolean>;
    fromDto(dto: EditUserDto): void;
}
