"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const constraints_1 = require("../../config/constraints");
const error_messages_1 = require("../messages/error.messages");
const edit_user_dto_1 = require("./dto/edit-user.dto");
const user_entity_1 = require("./user.entity");
const user_service_1 = require("./user.service");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    async editUser(dto, userId) {
        return this.userService.editUser(dto, userId);
    }
    async deactivate(userId) {
        return this.userService.deactivate(userId);
    }
    async reactivate(userId) {
        return this.userService.reactivateUser(userId);
    }
    uploadAvatar(avatarFile, userId) {
        return this.userService.saveAvatar(avatarFile, userId);
    }
    async getAvatar(userId, res) {
        const avatarPath = await this.userService.getAvatarPath(userId);
        res.sendFile(avatarPath, { root: constraints_1.avatarsFolder });
    }
};
__decorate([
    common_1.Post('/edit/:id'),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.userNotFound }),
    swagger_1.ApiCreatedResponse({ type: user_entity_1.User }),
    __param(0, common_1.Body()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [edit_user_dto_1.EditUserDto, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "editUser", null);
__decorate([
    common_1.Post('deactivate/:id'),
    swagger_1.ApiCreatedResponse({ type: user_entity_1.User }),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.userNotFound }),
    swagger_1.ApiBadRequestResponse({ description: error_messages_1.userAlreadyDeactivated }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deactivate", null);
__decorate([
    common_1.Post('/reactivate/:id'),
    swagger_1.ApiCreatedResponse({ type: user_entity_1.User }),
    swagger_1.ApiNotFoundResponse({ description: error_messages_1.userNotFound }),
    swagger_1.ApiBadRequestResponse({ description: error_messages_1.userAlreadyActivated }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "reactivate", null);
__decorate([
    common_1.Post('/avatar/:id'),
    swagger_1.ApiCreatedResponse(),
    swagger_1.ApiNotFoundResponse({ description: `${error_messages_1.userAlreadyDeactivated},${error_messages_1.userNotFound}` }),
    swagger_1.ApiBadRequestResponse({ description: `${error_messages_1.fileTooBig},${error_messages_1.avatarImageCanotBeEmpty}` }),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', { dest: constraints_1.tempAvatarsFolder })),
    __param(0, common_1.UploadedFile()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "uploadAvatar", null);
__decorate([
    common_1.Get('/avatar/:id'),
    swagger_1.ApiOkResponse({ type: 'avatar' }),
    swagger_1.ApiNotFoundResponse({ description: `${error_messages_1.userAlreadyDeactivated}, ${error_messages_1.userNotFound}, ${error_messages_1.avatarNotFound}` }),
    __param(0, common_1.Param('id')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getAvatar", null);
UserController = __decorate([
    common_1.Controller('user'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map