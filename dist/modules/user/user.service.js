"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const class_transformer_1 = require("class-transformer");
const constraints_1 = require("../../config/constraints");
const error_messages_1 = require("../messages/error.messages");
const user_entity_1 = require("./user.entity");
const fs = require("fs");
const user_repository_1 = require("./user.repository");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async save(user) {
        return this.userRepository.save(user);
    }
    async findUserByEmail(email) {
        return this.userRepository.findOne({ email }, { relations: ['card', 'address'] });
    }
    async findUserById(id) {
        const rawUser = await this.userRepository.query(`select * from  "user" u where u.id = '${id}'`);
        if (rawUser.length === 0) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        const user = class_transformer_1.plainToClass(user_entity_1.User, rawUser[0]);
        return user;
    }
    async validateUserPassword(dto) {
        const { email, password: password } = dto;
        const user = await this.userRepository.findOne({ email });
        return await user.validationPassword(password);
    }
    async editUser(dto, userId) {
        const databaseUser = await this.userRepository.findOne({ id: userId });
        if (!databaseUser) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        databaseUser.fromDto(dto);
        console.log(dto.address);
        return this.userRepository.save(databaseUser).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async reactivateUser(userId) {
        const databaseUser = await this.userRepository.findOne({ id: userId });
        if (!databaseUser) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        databaseUser.isActive = true;
        return this.userRepository.save(databaseUser).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async deactivate(userId) {
        const databaseUser = await this.userRepository.findOne({ id: userId });
        if (!databaseUser) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        databaseUser.isActive = false;
        return this.userRepository.save(databaseUser).catch((error) => {
            throw new common_1.InternalServerErrorException(error.message);
        });
    }
    async saveAvatar(avatarFile, userId) {
        const user = await this.userRepository.findOne(userId);
        if (!user) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        if (!user.isActive) {
            throw new common_1.ForbiddenException(error_messages_1.userDeactivated);
        }
        if (avatarFile.size == 0) {
            throw new common_1.BadRequestException(error_messages_1.avatarImageCanotBeEmpty);
        }
        if ((avatarFile.size / constraints_1.MegaByte) > constraints_1.maxAvatarSize) {
            fs.unlinkSync(avatarFile.path);
            throw new common_1.BadRequestException(error_messages_1.fileTooBig);
        }
        const oldPath = avatarFile.path;
        const newPath = `${constraints_1.avatarsFolder}/${user.id}.png`.replace(oldPath, '');
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                throw new common_1.InternalServerErrorException(err);
            }
        });
    }
    async getAvatarPath(userId) {
        const user = await this.userRepository.findOne(userId);
        if (!user) {
            throw new common_1.NotFoundException(error_messages_1.userNotFound);
        }
        if (!user.isActive) {
            throw new common_1.ForbiddenException(error_messages_1.userDeactivated);
        }
        const avatarPath = `${constraints_1.avatarsFolder}/${user.id}.png`;
        if (!fs.existsSync(avatarPath)) {
            throw new common_1.NotFoundException(error_messages_1.avatarNotFound);
        }
        return `${user.id}.png`;
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map