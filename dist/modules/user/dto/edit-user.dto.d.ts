import { Address } from 'src/modules/address/address.entity';
export declare class EditUserDto {
    name: string;
    email: string;
    address: Address;
}
