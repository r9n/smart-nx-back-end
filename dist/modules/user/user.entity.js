"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const constraints_1 = require("../../config/constraints");
const typeorm_1 = require("typeorm");
const address_entity_1 = require("../address/address.entity");
const base_entity_1 = require("../base.entity");
const card_entity_1 = require("../card/card.entity");
const bcrypt = require("bcrypt");
const roles_enum_1 = require("../../enums/roles.enum");
let User = class User extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
    async validationPassword(password) {
        const hash = await bcrypt.hash(password, this.salt);
        console.log(hash.toString() == this.passwordHash.toString());
        return (hash.toString() == this.passwordHash.toString());
    }
    fromDto(dto) {
        this.name = dto.name ? dto.name : this.name;
        this.email = dto.email ? dto.email : this.email;
        this.address = dto.address ? dto.address : this.address;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_USER_NAME_LENGHT }),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_USER_EMAIL_LENGHT }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean' }),
    __metadata("design:type", Boolean)
], User.prototype, "isActive", void 0);
__decorate([
    typeorm_1.OneToMany((type) => card_entity_1.Card, (card) => card.user, { cascade: true }),
    __metadata("design:type", Array)
], User.prototype, "card", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => address_entity_1.Address, (address) => address.users),
    __metadata("design:type", address_entity_1.Address)
], User.prototype, "address", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], User.prototype, "passwordHash", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], User.prototype, "salt", void 0);
__decorate([
    typeorm_1.Column({ type: 'enum', enum: roles_enum_1.Roles }),
    __metadata("design:type", String)
], User.prototype, "role", void 0);
User = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map