export declare abstract class BaseEntity {
    id: string;
    createDate: Date;
    updateDate: Date;
    version: number;
}
