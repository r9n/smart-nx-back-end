"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
const typeorm_1 = require("typeorm");
const base_entity_1 = require("../base.entity");
const user_entity_1 = require("../user/user.entity");
const city_entity_1 = require("./city/city.entity");
let Address = class Address extends base_entity_1.BaseEntity {
    constructor(partial) {
        super();
        Object.assign(this, partial);
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], Address.prototype, "street", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar' }),
    __metadata("design:type", String)
], Address.prototype, "postalCode", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => city_entity_1.City, (city) => city.address),
    __metadata("design:type", city_entity_1.City)
], Address.prototype, "City", void 0);
__decorate([
    typeorm_1.OneToMany((type) => user_entity_1.User, (user) => user.address),
    __metadata("design:type", user_entity_1.User)
], Address.prototype, "users", void 0);
Address = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Address);
exports.Address = Address;
//# sourceMappingURL=address.entity.js.map