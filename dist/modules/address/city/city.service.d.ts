import { City } from './city.entity';
import { CityRepository } from './city.repository';
export declare class CityService {
    private repo;
    constructor(repo: CityRepository);
    getCityByState(stateId: string): Promise<City[]>;
}
