import { BaseEntity } from 'src/modules/base.entity';
import { Address } from '../address.entity';
import { State } from '../state/state.entity';
export declare class City extends BaseEntity {
    city: string;
    state: State;
    address: Address[];
}
