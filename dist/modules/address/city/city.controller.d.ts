import { City } from './city.entity';
import { CityService } from './city.service';
export declare class CityController {
    private service;
    constructor(service: CityService);
    getCities(stateId: string): Promise<City[]>;
}
