"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.City = void 0;
const constraints_1 = require("../../../config/constraints");
const base_entity_1 = require("../../base.entity");
const typeorm_1 = require("typeorm");
const address_entity_1 = require("../address.entity");
const state_entity_1 = require("../state/state.entity");
let City = class City extends base_entity_1.BaseEntity {
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_CITY_NAME_LENGHT }),
    __metadata("design:type", String)
], City.prototype, "city", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => state_entity_1.State, (state) => state.cities),
    __metadata("design:type", state_entity_1.State)
], City.prototype, "state", void 0);
__decorate([
    typeorm_1.OneToMany((type) => address_entity_1.Address, (address) => address.City, { cascade: true }),
    __metadata("design:type", Array)
], City.prototype, "address", void 0);
City = __decorate([
    typeorm_1.Entity()
], City);
exports.City = City;
//# sourceMappingURL=city.entity.js.map