"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountryService = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const country_entity_1 = require("./country.entity");
const country_repository_1 = require("./country.repository");
let CountryService = class CountryService {
    constructor(repo) {
        this.repo = repo;
    }
    async getAllCountry() {
        return this.repo.find();
    }
};
__decorate([
    swagger_1.ApiOkResponse({ type: [country_entity_1.Country] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CountryService.prototype, "getAllCountry", null);
CountryService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [country_repository_1.CountryRepository])
], CountryService);
exports.CountryService = CountryService;
//# sourceMappingURL=country.service.js.map