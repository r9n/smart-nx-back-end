import { Country } from './country.entity';
import { CountryRepository } from './country.repository';
export declare class CountryService {
    private repo;
    constructor(repo: CountryRepository);
    getAllCountry(): Promise<Country[]>;
}
