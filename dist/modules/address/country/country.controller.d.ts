import { Country } from './country.entity';
import { CountryService } from './country.service';
export declare class CountryController {
    private countryService;
    constructor(countryService: CountryService);
    getCities(): Promise<Country[]>;
}
