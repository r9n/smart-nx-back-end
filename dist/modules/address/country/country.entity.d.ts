import { BaseEntity } from 'src/modules/base.entity';
import { State } from '../state/state.entity';
export declare class Country extends BaseEntity {
    country: string;
    states: State[];
}
