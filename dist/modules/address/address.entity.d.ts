import { BaseEntity } from '../base.entity';
import { User } from '../user/user.entity';
import { City } from './city/city.entity';
export declare class Address extends BaseEntity {
    street: string;
    postalCode: string;
    City: City;
    users: User;
    constructor(partial: Partial<Address>);
}
