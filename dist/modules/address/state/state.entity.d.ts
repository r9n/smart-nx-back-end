import { BaseEntity } from 'src/modules/base.entity';
import { City } from '../city/city.entity';
import { Country } from '../country/country.entity';
export declare class State extends BaseEntity {
    state: string;
    country: Country;
    cities: City[];
}
