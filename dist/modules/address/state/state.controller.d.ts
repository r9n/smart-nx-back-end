import { State } from './state.entity';
import { StateService } from './state.service';
export declare class StateController {
    private service;
    constructor(service: StateService);
    getStates(countrId: string): Promise<State[]>;
}
