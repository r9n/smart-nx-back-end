"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.State = void 0;
const constraints_1 = require("../../../config/constraints");
const base_entity_1 = require("../../base.entity");
const typeorm_1 = require("typeorm");
const city_entity_1 = require("../city/city.entity");
const country_entity_1 = require("../country/country.entity");
let State = class State extends base_entity_1.BaseEntity {
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: constraints_1.MAX_STATE_LENGHT }),
    __metadata("design:type", String)
], State.prototype, "state", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => country_entity_1.Country, (country) => country.states),
    __metadata("design:type", country_entity_1.Country)
], State.prototype, "country", void 0);
__decorate([
    typeorm_1.OneToMany((type) => city_entity_1.City, (city) => city.state, { cascade: true }),
    __metadata("design:type", Array)
], State.prototype, "cities", void 0);
State = __decorate([
    typeorm_1.Entity()
], State);
exports.State = State;
//# sourceMappingURL=state.entity.js.map