import { State } from './state.entity';
import { StateRepository } from './state.repository';
export declare class StateService {
    private repo;
    constructor(repo: StateRepository);
    getStateByCountry(country: string): Promise<State[]>;
}
