"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardBrands = void 0;
var CardBrands;
(function (CardBrands) {
    CardBrands["ELO"] = "elo";
    CardBrands["VISA"] = "visa";
    CardBrands["MASTERCARD"] = "mastercard";
    CardBrands["MAESTRO"] = "maestro";
    CardBrands["HIPERCARD"] = "hipercard";
})(CardBrands = exports.CardBrands || (exports.CardBrands = {}));
//# sourceMappingURL=card-brands.enum.js.map