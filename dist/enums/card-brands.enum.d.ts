export declare enum CardBrands {
    ELO = "elo",
    VISA = "visa",
    MASTERCARD = "mastercard",
    MAESTRO = "maestro",
    HIPERCARD = "hipercard"
}
