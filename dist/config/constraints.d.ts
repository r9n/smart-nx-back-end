export declare const API_VERION = "1.0.0";
export declare const API_TITLE = "SmartNx Api";
export declare const API_DESCRIPTION = "Uma api simples para gerenciamento de usu\u00E1rios e cart\u00F5es";
export declare const MAX_USER_NAME_LENGHT = 60;
export declare const MIN_USER_NAME_LENGHT = 3;
export declare const MAX_USER_EMAIL_LENGHT = 40;
export declare const MIN_USER_EMAIL_LENGHT = 7;
export declare const MAX_USER_PASSWORD = 8;
export declare const MIN_USER_PASSWORD = 3;
export declare const Kbyte = 1;
export declare const MegaByte: number;
export declare const maxAvatarSize: number;
export declare const avatarsFolder = "./avatars";
export declare const tempAvatarsFolder = "./tempAvatars";
export declare const MAX_CARD_HOLDER_NAME_LENGHT = 60;
export declare const MIN_CARD_HOLDER_NAME_LENGHT = 3;
export declare const MAX_CARD_NUMBER_LENGHT = 40;
export declare const MIN_CARD_NUMBER_LENGHT = 8;
export declare const MAX_CARD_BRAND_NAME_LENGHT = 16;
export declare const MIN_CARD_BRAND_NAME_LENGHT = 3;
export declare const MAX_CARD_SECURE_CODE_LENGHT = 6;
export declare const MIN_CARD_SECURE_CODE_LENGHT = 4;
export declare const MAX_CITY_NAME_LENGHT = 128;
export declare const MIN_CITY_NAME_LENGHT = 2;
export declare const MAX_STATE_LENGHT = 128;
export declare const MIN_STATE_LENGHT = 2;
export declare const MAX_COUNTRY_NAME_LENGHT = 64;
export declare const MIN_COUNTRY_NAME_LENGHT = 2;
export declare const MAX_ADDRESS_STREET_LENGHT = 64;
export declare const MIN_ADDRESS_STREE_LENGHT = 2;
export declare const MAX_ADDRESS_POSTALCODE_LENGHT = 64;
export declare const MIN_ADDRESS_POSTALCODE_LENGHT = 2;
