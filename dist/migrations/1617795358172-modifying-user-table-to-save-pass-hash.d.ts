import { MigrationInterface, QueryRunner } from "typeorm";
export declare class modifyingUserTableToSavePassHash1617795358172 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
