"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addingCard1617803214442 = void 0;
class addingCard1617803214442 {
    constructor() {
        this.name = 'addingCard1617803214442';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "card" DROP COLUMN "number"`);
        await queryRunner.query(`ALTER TABLE "card" ADD "number" character varying NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "card" DROP COLUMN "number"`);
        await queryRunner.query(`ALTER TABLE "card" ADD "number" character varying(60) NOT NULL`);
    }
}
exports.addingCard1617803214442 = addingCard1617803214442;
//# sourceMappingURL=1617803214442-adding-card.js.map