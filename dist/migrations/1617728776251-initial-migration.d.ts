import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class initialMigration1617728776251 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
