"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.modifyingUserTableToSavePassHash1617795358172 = void 0;
class modifyingUserTableToSavePassHash1617795358172 {
    constructor() {
        this.name = 'modifyingUserTableToSavePassHash1617795358172';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" ADD "passwordHash" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD "salt" character varying NOT NULL`);
        await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('admin', 'user')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "role" "user_role_enum" NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "role"`);
        await queryRunner.query(`DROP TYPE "user_role_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "salt"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "passwordHash"`);
    }
}
exports.modifyingUserTableToSavePassHash1617795358172 = modifyingUserTableToSavePassHash1617795358172;
//# sourceMappingURL=1617795358172-modifying-user-table-to-save-pass-hash.js.map