"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.populateCountryStateCity1617733069527 = void 0;
class populateCountryStateCity1617733069527 {
    async up(queryRunner) {
        await queryRunner.query(`insert  into country ("id","country") values('0814dedc-fc8f-472c-8ddb-b3f360034c66','Brasil');    `);
        await queryRunner.query(`insert into state ("id","state","countryId" ) values('33186a2f-81d8-4497-a564-84f9cc663469','Belo Horizonte','0814dedc-fc8f-472c-8ddb-b3f360034c66')`);
        await queryRunner.query(`insert into state ("id","state","countryId" ) values('c94625ab-b7e0-4ab0-b47c-73c13349a7d4','São Paulo','0814dedc-fc8f-472c-8ddb-b3f360034c66')`);
        await queryRunner.query(`insert into state ("id","state","countryId" ) values('7ed4c880-8317-4021-a448-5fbae84f8c89','Minas Gerais','0814dedc-fc8f-472c-8ddb-b3f360034c66')`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('127ecd44-4f32-4aad-bd09-a8b0224e5e48','Aparecida','33186a2f-81d8-4497-a564-84f9cc663469');
`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('6ec805c8-f034-4862-9a36-8bd6ad986779','Lagoa','33186a2f-81d8-4497-a564-84f9cc663469')`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('7c928bc2-c0f2-4847-a88b-0532d90cb00b','São José dos Campos','c94625ab-b7e0-4ab0-b47c-73c13349a7d4')`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('d9438a10-6f23-474e-a75f-ee0545f1963c','São Paulo','c94625ab-b7e0-4ab0-b47c-73c13349a7d4')`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('c6e4b1d1-2d87-4233-90ef-376d520621a8','Bicas','7ed4c880-8317-4021-a448-5fbae84f8c89')`);
        await queryRunner.query(`insert into city ("id","city","stateId" ) values('1733088c-6a89-4492-9053-c6a6b42b29d9','Juiz de Fora','7ed4c880-8317-4021-a448-5fbae84f8c89')`);
    }
    async down(queryRunner) {
        await queryRunner.query(`delete from city`);
        await queryRunner.query(`delete from state`);
        await queryRunner.query(`delete from country`);
    }
}
exports.populateCountryStateCity1617733069527 = populateCountryStateCity1617733069527;
//# sourceMappingURL=1617733069527-populate-country-state-city.js.map